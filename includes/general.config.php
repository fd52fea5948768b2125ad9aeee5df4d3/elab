<?php

$NAVBAR_TEXT = "eLab";
$HREF_URL = "#";
$TITLE_TEXT = "eLab";

$Cversion ="GCC v4.8.4";
$CPPversion ="GCC v4.8.4";
$JAVAversion ="JAVA SE 8";
$MATHSLABversion ="OCTAVE v4.0.2";
$PYTHONversion ="PYTHON v3.6";
$PERLversion ="PERL v5.18.2";
$Rversion ="R v3.4.1";
$CSHARPversion ="MONO JIT v5.2.0";
$HASKELLversion ="GHC v7.6.3";
$RUBYversion ="RUBY v1.9.3";

$FULLSCREEN=" (Press F6 for Full Screen)";
?>
