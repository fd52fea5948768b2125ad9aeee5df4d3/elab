
<?php
   session_start();
    if(!isset($_SESSION['uname']) || $_SESSION['role'] != 'D') {
        echo "ERROR IN SESSION";
        exit;
    }
    include_once("../../../includes/sql.config.php");
    include_once("../../../includes/general.config.php");
	  $courseid=$_GET['courseid'];
	  $levelid=$_GET['levelid'];
	  $coursename=$_GET['coursename'];
	  $FACULTY_ID=$_GET['faculty_id'];
?>
<html>
    <head>
      <!--Import materialize.css-->
          <title>Charts</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <script src="../../../js/jquery-3.1.0.min.js" type="text/javascript"></script>
          <link type="text/css" rel="stylesheet" href="../../../css/materialize.min.css" media="screen,projection" />
          <link href="../../../css/material-icons.css" rel="stylesheet">
         <script type="text/javascript" src="../../../js/loader.js"></script>

        <style>
        .container {
            position: relative;
           top: 10%;

        }    
        .login {
            padding: 20px;
            
            margin-bottom: 0px;
        }        
        .cbox {
            padding: 2px;
            font-size: 4em;
            
        }
        .htitle {
          padding-top: 25px;
        }
        .btnpaddin
        {
             margin-top: 25px !important;
        }

        
        </style>
    </head>

    <body>


        <nav>
            <div class="nav-wrapper indigo darken-1">
                <a href="<?php echo $HREF_URL  ?>"><img id="image" class="brand-logo logo-img s2" src="../../../logo.png"> </img></a>
                </a>
                <a href="#" class="brand-logo  center hide-on-med-and-down"><?php echo $NAVBAR_TEXT; ?></a>
                <ul id="nav-mobile" class="right">
                    <li><a href="../../../index.php">Logout</a></li>
                     <li><a href="index.php">Back</a></li>
                </ul>
            </div>
        </nav>
    
     
    <div class="container">
      <div class="row">
        <div class="col s12">
          <div class="card z-depth-2">
             <div class="login indigo darken-1 flow-text white-text"> CHARTS</div>
            <div class="card-content">
              <div class="form">
                <div class="row">
                	<script type="text/javascript">
                    google.charts.load("visualization", "1", {packages:["corechart"]});
                    google.charts.setOnLoadCallback(drawChart);
                  function drawChart() {
                        
                    var data = google.visualization.arrayToDataTable([

                        ['Student No', 'Completed'],
                          <?php 
                 $query = "SELECT STUD_ID, LEVEL_$levelid FROM faculty_$FACULTY_ID WHERE COURSE_ID=$courseid";

                          $exec = mysqli_query($link,$query);
                          while($row = mysqli_fetch_array($exec)){
                          	if($levelid==1)
                            	echo "['".$row['STUD_ID']."',".$row['LEVEL_1']."],";
                        	else if($levelid==2)
                        		echo "['".$row['STUD_ID']."',".$row['LEVEL_2']."],";
                        	else if($levelid==3)
                        		echo "['".$row['STUD_ID']."',".$row['LEVEL_3']."],";
                          }
                   ?>
                       
                      ]);

                      var options = {
                        title: '',
                        vAxis: {
          				      title: 'Completed (1-100)',
                         viewWindow: {
                                      min: [0],
                                      max: [100]
                                     }
        				        },
                				hAxis: {
                  				title: 'Student Register Number'
                				},
                        };
                    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart"));
                    chart.draw(data, options);
                }
            </script>


                <div class="row">
      			<div class="col s12 center flow-text ">
              <?php echo "COURSE : ".$coursename." "; ?>
              <?php echo "& LEVEL : ".$levelid; ?><br>
              <?php echo "FACULTY ID : ".$FACULTY_ID; ?>
      			</div>
    			</div>

             <div id="columnchart" style="width: auto; height: 500px;"></div>


                  </div>
                </div>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
               
   <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="../../../js/jquery-3.1.0.min.js"></script>
      <script type="text/javascript" src="../../../js/materialize.min.js"></script>
    </body>
  </html>
