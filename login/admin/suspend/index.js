$(document).ready(function(){

    Materialize.toast('SUSPEND STUDENT SECTION!', 1000, "rounded");
    displayTable();

    $("#sTable").html("");


      function displayTable() {
        $.ajax({
            type: 'POST',
            url: 'student.helper.php',
            success: function(phpdata) { 
                $("#sTable").html(phpdata);
            }
        });
    }


 	   $("#viewbtn").click(function() {

			var regno = $('#regno').val();
            var chk =1;
	        $.ajax({
                type: 'POST',
                data: {
                  'regno': regno,
                  'chk':chk,
                },
                url: 'index.helper.php',
                beforeSend: function(){
                 
                },        
                success: function(final_result) 
                {
                    if(final_result === '0')
                    {
                     Materialize.toast('Student Successfully Suspended!', 4000, "Green rounded");
                     displayTable();
                    }
                    else if(final_result === '1')
                    {
                     Materialize.toast('Student Not Suspended', 4000, "red rounded");
                    }
                    else if(final_result === '2')
                    {
                     Materialize.toast('Student is Already Suspended', 4000, "red rounded");
                    }             
                }
        });

 	   });

       $("#viewbtn1").click(function() {

            var regno1 = $('#regno1').val();
            var chk =2;
            $.ajax({
                type: 'POST',
                data: {
                  'regno': regno1,
                  'chk':chk,
                },
                url: 'index.helper.php',
                beforeSend: function(){
                },        
                success: function(final_result) 
                {
                    if(final_result === '0')
                    {
                     Materialize.toast('Student Successfully Re-Enrolled!', 4000, "Green rounded");
                     displayTable();
                    }
                    else if(final_result === '1')
                    {
                     Materialize.toast('Student Not Re-Enrolled', 4000, "red rounded");
                    }
                    else if(final_result === '3')
                    {
                     Materialize.toast('Student is Already Re-Enrolled', 4000, "red rounded");
                    }                 
                }
        });

       });



});
