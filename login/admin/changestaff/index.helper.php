<?php
    session_start();
    if($_SESSION['role'] != 'A') 
    {
    echo "ERROR IN SESSION";
    exit;
    }

    $final_result = Array();
    $final_result['error'] = 0;
    $stat=-1;
  
    
    include_once(__DIR__."/../../../includes/sql.config.php");
    include_once(__DIR__."/../../../includes/general.config.php");

    $regno = trim($_REQUEST['regno']);
    $courseid=  trim($_REQUEST['courseid']);
    $old_facultyid=  trim($_REQUEST['ostaffid']);
    $new_facultyid=  trim($_REQUEST['nstaffid']);
    $scid=$regno.$courseid;


     //student table
     $stdtable = "std_db_".$regno;
     $sql1="UPDATE $stdtable set `STAFF_ID` = '$new_facultyid' WHERE SEQUENCE_ID LIKE '$courseid%' AND `STAFF_ID`= '$old_facultyid'";
     $db1 = mysqli_query($link,$sql1);
     if(!$db1){
        $stat = 6;
        return;
     }
     else
         $stat=0; 

     //pick data from current faculty
     $stafftable = "faculty_".$old_facultyid;
     $sql2 = "SELECT STUD_ID,STUD_NAME,COURSE_NAME,COURSE_ID,LEVEL_1,LEVEL_2,LEVEL_3,REQ_ID FROM $stafftable WHERE `REQ_ID` LIKE '$scid'";
     $db2 = mysqli_query($link,$sql2);

     if(!$db2) {
        echo  $stat=5;
        return;
          
     }

     if(mysqli_num_rows($db2) > 0) {
        if(mysqli_query($link,$sql2)) {
            $row = mysqli_fetch_assoc($db2);
            $STUD_ID = $row['STUD_ID'];
            $STUD_NAME = $row['STUD_NAME'];
            $COURSE_NAME = $row['COURSE_NAME'];
            $COURSE_ID = $row['COURSE_ID'];
            $LEVEL_1 = $row['LEVEL_1'];
            $LEVEL_2 = $row['LEVEL_2'];
            $LEVEL_3 = $row['LEVEL_3'];
            $stat=0;  
        }
          
    } else {
        echo  $stat=2;
        return;
    }

     //insert data to new faculty table
     $nstafftable = "faculty_".$new_facultyid;
     $sql3="INSERT INTO $nstafftable (STUD_ID,STUD_NAME,COURSE_NAME,COURSE_ID,LEVEL_1,LEVEL_2,LEVEL_3,REQ_ID) VALUES ('$STUD_ID','$STUD_NAME','$COURSE_NAME',$COURSE_ID,$LEVEL_1,$LEVEL_2,$LEVEL_3, '$scid')";
     $db3 = mysqli_query($link,$sql3);
     if(!$db3)
     {
            $stat = 4;
            return;
     }
     else
         $stat=0; 



     // delete record in  from current faculty
     $stafftable = "faculty_".$old_facultyid;
     $sql2 = "DELETE FROM $stafftable WHERE `REQ_ID` LIKE '$scid'";
     $db2 = mysqli_query($link,$sql2);
     if(!$db2){
        $stat = 2;
        return;
     }
     else
         $stat=0;


    //course_request_table
    $sql = "UPDATE `course_reg_table` SET `STAFF_ID`='$new_facultyid' WHERE `REQ_ID` LIKE '$scid'";
    $db = mysqli_query($link,$sql);
    if(!$db){
        $stat = 1;
        return;
    }
    else
         $stat=0;    

    echo $stat;

    ?>