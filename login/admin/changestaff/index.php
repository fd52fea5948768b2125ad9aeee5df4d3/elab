<?php
    session_start();
    if($_SESSION['role'] != 'A') 
    {
    echo "ERROR IN SESSION";
    exit;
    }
    include_once(__DIR__."/../../includes/sql.config.php");
    include_once(__DIR__."/../../includes/general.config.php");
    $username =  $_SESSION['uname'];
    $name = $_SESSION['name'];
    define('USERNAME',$username);
    $TABLE_NAME = "course_req_table";
    

    
    include_once(__DIR__."/../../../includes/sql.config.php");
    include_once(__DIR__."/../../../includes/general.config.php");

    $sql = "SELECT `USER_ID`, `FIRST_NAME`, `LAST_NAME`, `MAIL_ID`, `MOBILE`, `ROLE`, `PASSWORD`, `DOB`, `DEPT` FROM `users_table` WHERE `USER_ID` LIKE '$username';";

    $db = mysqli_query($link,$sql);

    if(!$db)
          die("Failed to Load: ".mysqli_error($link));

    $row = mysqli_fetch_assoc($db);
    $date_db = date_create_from_format("Y-m-d",$row['DOB']);
    $date = date_format($date_db, "j F, Y");
?>
<html>
    <head>
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="../../../css/materialize.min.css"  media="screen,projection"/>
      <link href="../../../css/material-icons.css" rel="stylesheet">
            <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <style>
        nav div a img.logo-img {
              height: 100%;
              padding: 4px;
              margin-left: 40px;
          }
        .container {
           position: relative;
           top: 5%;
        }    
         .htitle {
          margin-top: 50px;
        }
        .login {
            padding: 20px;
            margin-bottom: 0px;
        }
        .fontsize
        {
           font-size: 1rem !important;
        }
        .fontsizetxt
        {
           font-size: 0.8rem !important;
        }
        .padbot
        {
          margin-top: 10px;
          margin-bottom: 0px;
        }
        .txtbot
        {
          text-transform: uppercase;
        }
        .title
        {
            font-size: 2em;
            color: #43a047;
            margin-bottom: 0px;
        }
        .iconsize
        {
          font-size: 30px !important;
        }
        </style>
    </head>

    <body>

        <nav>
            <div class="nav-wrapper indigo darken-1">
                <a href="<?php echo $HREF_URL  ?>"><img id="image" class="brand-logo logo-img s2" src="../../../logo.png"> </img></a>
                </a>
                <a href="#" class="brand-logo  center hide-on-med-and-down"><?php echo $NAVBAR_TEXT; ?></a>
                <ul id="nav-mobile" class="right">
                    <li><a href="../../../index.php">Logout</a></li>
                    <li><a href="../home.php">Home</a></li>
                </ul>
            </div>
        </nav>
    
     
    <div class="container">
      <div class="row">
        <div class="col s12">
          <div class="card z-depth-2">
                        <div class="login indigo darken-1 flow-text txtbot white-text"> Change Faculty for Student
                </div>
            <div class="card-content">

              <div class="form">
                

                <div class="row section">
                     
                     <div class="input-field col s6">
                      <i class="teal-text  material-icons prefix">person</i>
                          <input id="regno" type="text" name="username" class="validate"">
                          <label class="grey-text fontsizetxt" for="regno">Student Registration Number</label>
                     </div>
                                
                                <div class="input-field col s6">
                                  <i class="teal-text  material-icons prefix">view_module</i>
                                    <select id="courseid">
                                    <option value="" disabled selected>Choose Course</option>
                                    <?php
                                        $sql_regid = "SELECT COURSE_ID,COURSE_NAME FROM COURSE_TABLE ORDER BY COURSE_ID";
                                        $db_regid = mysqli_query($link,$sql_regid);
                                        if(!$db_regid)
                                            die("Failed to Insert: ".mysqli_error($link));
                                        if(mysqli_num_rows($db_regid) > 0) {
                                            if(mysqli_query($link,$sql_regid)) {
                                                while($row = mysqli_fetch_assoc($db_regid)){
                                                    $temp1 = $row['COURSE_ID'];
                                                    $temp2 = $row['COURSE_NAME'];
                                                    echo "<option value=\"".$row['COURSE_ID']."\">".$temp1." - ".$temp2."</option>";
                                                }
                                            }
                                        }
                                    ?>
                                    </select>
                                    <label>Select Course</label>
                                </div>

                    <div class="input-field col s6">
                      <i class="teal-text  material-icons prefix">account_circle</i>
                          <input id="ostaffid" type="text" name="username" class="validate"">
                          <label class="grey-text fontsizetxt" for="regno">Current Faculty ID</label>
                     </div>

                    <div class="input-field col s6">
                      <i class="teal-text  material-icons prefix">person_add</i>
                          <input id="nstaffid" type="text" name="username" class="validate"">
                          <label class="grey-text fontsizetxt" for="regno">New Faculty ID</label>
                     </div>

                     <div class="col s12">
                         <a class=" btn-large pink right" id="viewbtn">SUBMIT REQUEST</a>
                      </div>   
                    </div>  

                </div>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
               
	 <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="../../../js/jquery-3.1.0.min.js"></script>
      <script type="text/javascript" src="../../../js/materialize.min.js"></script>
      <script type="text/javascript" src="index.js" ></script>
    </body>
  </html>