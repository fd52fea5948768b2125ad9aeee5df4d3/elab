$(document).ready(function(){
  $('#errorMsg').hide();
      $('select').material_select();


 Materialize.toast('Change Faculty for Student !', 1000);

 	   $("#viewbtn").click(function() {

			var regno = $('#regno').val();
            
            var courseid = $('#courseid :selected').val();
             if(courseid === "") 
             {
                Materialize.toast('Select your courseid', 1000, 'red rounded');
                return false;
             }
            var ostaffid = $('#ostaffid').val();
            var nstaffid = $('#nstaffid').val();

        if(regno.length === 0 ) {
            Materialize.toast('Enter your Registration Number', 1000, 'red rounded');
            return false;
        }
        if(ostaffid.length === 0 ) {
            Materialize.toast('Enter your Current Faculty ID', 1000, 'red rounded');
            return false;
        }
        if(nstaffid.length === 0 ) {
            Materialize.toast('Enter your New Faculty ID', 1000, 'red rounded');
            return false;
        }

	        $.ajax({
                type: 'POST',
                data: {
                  'regno': regno,
                  'courseid': courseid,
                  'ostaffid': ostaffid,
                  'nstaffid': nstaffid,
                },
                url: 'index.helper.php',
                //dataType: 'json',
                beforeSend: function(){
                 
                },        
                success: function(final_result) 
                {
                    if(final_result === '0')
                    {
                      Materialize.toast('Successfully Changed Your Faculty!',4000, "Green rounded");
                    }
                    else if(final_result === '1')
                    {
                      Materialize.toast('Error in Course Request Table!', 1000, "red rounded");
                    }
                    else if(final_result === '2')
                    {
                      Materialize.toast('No record in Current Faculty Table!', 1000, "red rounded");
                    }
                    else if(final_result === '4')
                    {
                      Materialize.toast('New Faculty Not Found!', 1000, "red rounded");
                    }
                    else if(final_result === '5')
                    {
                      Materialize.toast('Current Faculty Not Found!', 1000, "red rounded");
                    }
                    else if(final_result === '6')
                    {
                      Materialize.toast('Student Not Found!', 1000, "red rounded");
                    }

                }
        });



 	   });





});
