<?php
session_start();
if(!isset($_SESSION['uname']) || $_SESSION['role'] != 'C') {
    echo "ERROR IN SESSION";
    exit;
}
$username = $_SESSION['uname'];
$name = $_SESSION['name'];
$course_name = $_SESSION['course_name'];
$course_id = $_SESSION['course_id'];

include_once(__DIR__."/../../includes/sql.config.php");
include_once(__DIR__."/../../includes/general.config.php");
?>

<html>
<head>
    <title>e Lab Staff Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../css/materialize.min.css" type="text/css" />
    <script src="../../js/jquery-3.1.0.min.js" type="text/javascript"></script>
    <script src="student.js" type="text/javascript"></script>
</head>
<style>
    .srm-text {
        margin-left: 30px;
    }

    nav div a img.logo-img {
        height: 100%;
        padding: 4px;
        margin-left: 40px;
    }
    .login {
        padding: 20px;
        font-size: 1.3em;

        text-transform: uppercase;
    }
    .text {
        font-size: 1em;
    }
    .title {
        margin: 50px;
        margin-bottom: 10px;
        margin-left: 0;
    }
    .btn {
        margin-top: 23px;
    }
    .switch label input[type=checkbox]:checked+.lever:after {
        background-color: #FFF;
    }
</style>
<body>
<nav>
    <div class="nav-wrapper indigo darken-1">
        <a href="<?php echo $HREF_URL  ?>"><img id="image" class="brand-logo logo-img s2" src="../../logo.png"> </img></a>
        </a>
        <a href="#" class="brand-logo  center hide-on-med-and-down"><?php echo $NAVBAR_TEXT; ?></a>
        <ul id="nav-mobile" class="right">
            <li><a href="../../index.php">Logout</a></li>
            <li><a href="home.php">Home</a></li>
        </ul>
    </div>
</nav>


<div class="container">
    <div class="row">
        <div class="card">
            <div class="login indigo darken-1 white-text">
                Modify Student Question
            </div>
            <div class="card-content text">
                <div class="row">
                    <div class="col s12">
                        <div class="row">
                            <div class="col s6 input-field">
                                <input type="text" id="studentIDInput">
                                <label for="studentIDInput">Registration Number</label>
                            </div>

                            <div class="col s6 input-field">
                                <input type="text" id="questionNumberInput">
                                <label for="questionNumberInput">Student's Question Number</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12">
                        <a class="right-align right twaves-effect pink waves-light btn" id="editQuestionButton">Edit</a>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <div class="row" id="main_form_div"> <!-- REST BODY -->
                    <div class="row"> <!-- Session Row -->
			<div class="col s12 m4 input-field"> <!-- Question Row -->
                            <input  id="question_number" type="text" class="validate">
                            <label for="question_number">Question Number</label>
                        </div>  <!-- Question Number Row -->

                        <div class="col s12 m8 input-field"> <!-- Question Row -->
                            <input  id="question_name" type="text" class="validate">
                            <label for="question_name">Question Name</label>
                        </div>  <!-- Question Row -->

		    </div>


                    <div class="row"> <!-- Question Description Row -->
                        <div class="input-field col s12">
                            <textarea id="question_description" class="materialize-textarea"></textarea>
                            <label for="question_description">Question Description</label>
                        </div>
                    </div> <!-- Question Description Row -->
                    <div class="red-text text"><p>NOTE: Set Test Cases to 0, If not needed</p><p>INPUT AND OUTPUT is MUST</p></div>
                    <div class="row"> <!-- Test Case 1 Row -->
                        <div class="col s12 m6 input-field">
                            <textarea id="testcase1_input" class="materialize-textarea"></textarea>
                            <label for="testcase1_input">Test Case 1 INPUT</label>
                        </div>
                        <div class="col s12 m6 input-field">
                            <textarea id="testcase1_output" class="materialize-textarea"></textarea>
                            <label for="testcase1_output">Test Case 1 OUTPUT</label>
                        </div>
                    </div> <!-- Test Case 1 Row -->

                    <div class="row"> <!-- Test Case 2 Row -->
                        <div class="col s12 m6 input-field">
                            <textarea id="testcase2_input" class="materialize-textarea"></textarea>
                            <label for="testcase2_input">Test Case 2 INPUT</label>
                        </div>
                        <div class="col s12 m6 input-field">
                            <textarea id="testcase2_output" class="materialize-textarea"></textarea>
                            <label for="testcase2_output">Test Case 2 OUTPUT</label>
                        </div>
                    </div> <!-- Test Case 2 Row -->

                    <div class="row"> <!-- Test Case 3 Row -->
                        <div class="col s12 m6 input-field">
                            <textarea id="testcase3_input" class="materialize-textarea"></textarea>
                            <label for="testcase3_input">Test Case 3 INPUT</label>
                        </div>
                        <div class="col s12 m6 input-field">
                            <textarea id="testcase3_output" class="materialize-textarea"></textarea>
                            <label for="testcase3_output">Test Case 3 OUTPUT</label>
                        </div>
                    </div> <!-- Test Case 3 Row -->

                    <div class="row"> <!-- Test Case 4 Row -->
                        <div class="col s12 m6 input-field">
                            <textarea id="testcase4_input" class="materialize-textarea"></textarea>
                            <label for="testcase4_input">Test Case 4 INPUT</label>
                        </div>
                        <div class="col s12 m6 input-field">
                            <textarea id="testcase4_output" class="materialize-textarea"></textarea>
                            <label for="testcase4_output">Test Case 4 OUTPUT</label>
                        </div>
                    </div> <!-- Test Case 4 Row -->

                    <div hidden="hidden" class="row"> <!-- Test Case 5 Row -->
                        <div hidden="hidden"  class="col s12 m6 input-field">
                            <textarea hidden="hidden" id="testcase5_input" class="materialize-textarea"></textarea>
                            
                        </div>
                        <div hidden="hidden"  class="col s12 m6 input-field">
                            <textarea hidden="hidden"  id="testcase5_output" class="materialize-textarea"></textarea>
                            
                        </div>
                    </div> <!-- Test Case 5 Row -->


                </div> <!-- REST BODY -->
                <div class="row center" id = "add_question_button_div">
                    <a id="addQuestionBtn" class="waves-effect pink waves-light btn">Save</a>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- BASIC SETUP (DO NOT CHANGE) -->
<script type="text/javascript" src="../../js/materialize.min.js"></script>
<!-- DONT CHANGE ABOVE IT -->
</body>

</html>
