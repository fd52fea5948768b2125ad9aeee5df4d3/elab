<?php
    session_start();
    if(!isset($_SESSION['uname']) || $_SESSION['role'] != 'C') {
        echo "ERROR IN SESSION"; 
        exit;
    }

    $username = $_SESSION['uname'];
    $name = $_SESSION['name'];
    $course_name = $_SESSION['course_name'];
    
    
    include_once(__DIR__."/../../includes/sql.config.php");
    include_once(__DIR__."/../../includes/general.config.php");
    $TABLE_NAME = $course_name."_QUESTION_TABLE";
    $sql = "SELECT `Q_ID`,`S_NAME`,`Q_NAME` FROM `$TABLE_NAME`";

    $db = mysqli_query($link,$sql);
        if(!$db) 
            die("Failed to Insert: ".mysqli_error($link));       
            
    
    $sql_count = "SELECT (SELECT count(*) from `$TABLE_NAME` WHERE `Q_ID` LIKE '____1%') AS LEVEL1, (SELECT count(*) from `$TABLE_NAME` WHERE `Q_ID` LIKE '____2%') AS LEVEL2, (SELECT count(*) from `$TABLE_NAME` WHERE `Q_ID` LIKE '____3%') AS LEVEL3";
    $db_count = mysqli_query($link,$sql_count);
        if(!$db) 
            die("Failed to Get COUNT: ".mysqli_error($link));
            
    $row_counts = mysqli_fetch_assoc($db_count);
    
    
?>

    <html>

    <head>
        <title>View Question</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../../js/jquery-3.1.0.min.js" type="text/javascript"></script>
        <script src="home.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection" />
    </head>
    <style>
        .srm-text {
            margin-left: 30px;
        }
        
        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
            margin-left: 40px;
        }
        
        .card-heading-main {
            padding: 20px;
            font-weight: 300;
            margin-bottom: 15px;
        }
        
         .text {
            font-size: 1.2em;
        }
        
        .title {
            font-size: 1.8em;            
        }
        @media only screen and (max-width : 992px) {
            nav div a img.logo-img {
                margin-left: 0;
            }
        }
        .card-panel:hover {
            margin: 0;
            background-color: #FFF;
            cursor: pointer;
            box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
        }
        .dropdown-regid {
            width: 100%;
        }
        .resultDiv {
            padding: 15px;
            font-size: 3em;
        }
        .clear {
            clear: both;
        }
        .caps{

            text-transform: uppercase;
        }
    </style>

    <body>
        <nav>
            <div class="nav-wrapper indigo darken-1">
                <a href="<?php echo $HREF_URL  ?>"><img id="image" class="brand-logo logo-img s2" src="../../logo.png"> </img></a>
                </a>
                <a href="#" class="brand-logo  center hide-on-med-and-down"><?php echo $NAVBAR_TEXT; ?></a>
                <ul id="nav-mobile" class="right">
                    <li><a href="../../index.php">Logout</a></li>
                    <li><a href="home.php">Home</a></li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <div class="card">
                <p id="top-bar" class="card-heading-main caps indigo darken-1 white-text title"><?php echo $course_name." " ?> Questions</p>
                <div class="card-content">
                    <p class="caps title">Available Questions - <?php echo mysqli_num_rows($db); ?></p>
                    <br>
                    <div class="row caps">

                        <div class="col s12 m4">
                            <div class="card">
                                <p class="card-heading-main indigo darken-1 center white-text text">Level 1 Questions</p>
                                <div class="card-content center-align">
                                    <a id="l1" class="black-text  resultDiv"> <?php echo $row_counts['LEVEL1']; ?> </a>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m4">
                            <div class="card">
                                <p class="card-heading-main indigo darken-1 center white-text text">Level 2 Questions</p>
                                <div class="card-content center-align">
                                    <a id="l2" class="black-text resultDiv"> <?php echo $row_counts['LEVEL2']; ?> </a>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m4">
                            <div class="card">
                                <p class="card-heading-main indigo darken-1 center white-text text">Level 3 Questions</p>
                                <div class="card-content center-align">
                                    <a id="l3" class="black-text resultDiv"> <?php echo $row_counts['LEVEL3']; ?> </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                     <div class="col s12 tableDiv">
                            <?php
                                    echo "<table id=\"table_display_data\" class=\"bordered centered highlight\"><thead class=\"indigo white-text\"><tr><th>QUESTION ID</th><th>LEVEL</th></th><th>QUESTION NAME</th><th>SESSION NAME</th><th>SOLVE QUESTION</th></tr></thead><tbody>";
                                    if(mysqli_num_rows($db) > 0) {
                                        if(mysqli_query($link,$sql)) {
                                            while($row = mysqli_fetch_assoc($db)) {
                                                $Question_ID = $row['Q_ID'];
                                                $LEVEL = $Question_ID[4];
                                                echo "<tr>";
                                                echo "<td>".$row['Q_ID']."</td>";
                                                echo "<td>"."Level $LEVEL"."</td>";
                                                echo "<td>".$row['Q_NAME']."</td>";
                                                echo "<td>".$row['S_NAME']."</td>";
                                                echo "<td><a data-question='$Question_ID' data-course='$course_name' class='btn pink solveBtn'>Solve</a></td>";
                                                echo "</tr>";
                                            }
                                        }
                                    }
                                    echo "</tbody><table>";

                            ?>
                        </div>
                    </div>
                    <div>
                       <br><br> <div class="right-align col s12">
                                <a id="getReportBtn" class="hidden waves-effect pink waves-light btn-large">Export Questions</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
$('#getReportBtn').hide();

$('#getReportBtn').click(function(){
        var f = $("<form target='_blank' method='POST' style='display:none;'></form>").attr({
                action: '../export.data.php'
            }).appendTo(document.body);
        $('<input type="hidden" />').attr({
            name: 'html_data',
            value: $('.tableDiv').html()
        }).appendTo(f);

    f.submit();
    f.remove();
    });

$(".solveBtn").click(function(){
    var question_no = $(this).attr('data-question');
    var course_name = (""+$(this).attr('data-course')).toLowerCase();
    if(question_no == "") {
        window.alert("INVALID QUESTION NUMBER");
    }else {
        //Redirect to New Page
        window.location = "./test/"+course_name+"/"+course_name+".code.php?q="+question_no;
    }
});
        </script>


 


        <!-- BASIC SETUP (DO NOT CHANGE) -->
        <script type="text/javascript" src="../../js/materialize.min.js"></script>
        <!-- DONT CHANGE ABOVE IT -->
    </body>

    </html>