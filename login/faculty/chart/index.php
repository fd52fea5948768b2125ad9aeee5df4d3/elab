<?php
   session_start();
    if(!isset($_SESSION['uname']) || $_SESSION['role'] != 'F') {
        echo "ERROR IN SESSION";
        exit;
    }

    $FACULTY_ID =  $_SESSION['uname'];    
    include_once("../../../includes/sql.config.php");
    include_once("../../../includes/general.config.php");
?>
<html>
    <head>
      <!--Import materialize.css-->
          <title>Charts</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <script src="../../../js/jquery-3.1.0.min.js" type="text/javascript"></script>
          <link type="text/css" rel="stylesheet" href="../../../css/materialize.min.css" media="screen,projection" />
          <link href="../../../css/material-icons.css" rel="stylesheet">
         <script type="text/javascript" src="../../../js/loader.js"></script>
        <style>
        .container {
            position: relative;
           top: 10%;

        }    
        .login {
            padding: 20px;
            
            margin-bottom: 0px;
        }        
        .cbox {
            padding: 2px;
            font-size: 4em;
            
        }
        .htitle {
          padding-top: 25px;
        }
        .btnpaddin
        {
             margin-top: 25px !important;
        }

        
        </style>
    </head>

    <body>


        <nav>
            <div class="nav-wrapper indigo darken-1">
                <a href="<?php echo $HREF_URL  ?>"><img id="image" class="brand-logo logo-img s2" src="../../../logo.png"> </img></a>
                </a>
                <a href="#" class="brand-logo  center hide-on-med-and-down"><?php echo $NAVBAR_TEXT; ?></a>
                <ul id="nav-mobile" class="right">
                    <li><a href="../../../index.php">Logout</a></li>
                    <li><a href="../home.php">Home</a></li>
                </ul>
            </div>
        </nav>
    
     
    <div class="container">
      <div class="row">
        <div class="col s12">
          <div class="card z-depth-2">
             <div class="login indigo darken-1 flow-text white-text"> CHARTS</div>
            <div class="card-content">
              <div class="form">
                <div class="row">

                        <div class="col s3">
                         <div  class="input-field col s12">
                            <select id="courselist">
                              <option value="" disabled selected>Select Course</option>
                              <?php
                                      $TABLENAME= "faculty_".$FACULTY_ID;
                                      $sql = "SELECT DISTINCT COURSE_NAME,COURSE_ID FROM `$TABLENAME`";
                                      $db = mysqli_query($link,$sql);
                                      if(!$db)
                                        die("Failed to Select: ".mysqli_error($conn));
                                      $row = null;
                                      if(mysqli_num_rows($db) > 0 )
                                      {
                                      while ($row = mysqli_fetch_assoc($db))
                                        {
                                        echo "<option>".$row['COURSE_ID']."_".$row['COURSE_NAME']."</option>";
                                        }
                                      }
                              ?>  
                            </select>
                            <label>Course Selection</label>
                          </div>
                        </div>


                        <div class="col s3">
                         <div  class="input-field col s12">
                            <select id="levellist">
                              <option value="" disabled selected>Select Level</option>
                              <option value="" selected>Level_1</option>
                              <option value="">Level_2</option>
                              <option value="">Level_3</option>
                            </select>
                            <label>Level Selection</label>
                          </div>
                        </div>

                        <div class="col s3 btnpaddin center">
                          <a id="viewdetailsbtn" class="waves-effect waves-light hvr-bounce-to-left pink btn" >BAR CHART</a>
                        </div>      
                        <div class="col s3 btnpaddin center">
                          <a id="viewdetailsbtn2" class="waves-effect waves-light hvr-bounce-to-left pink btn" >PIE CHART</a>
                        </div>  








                  </div>
                </div>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
               
   <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="../../../js/jquery-3.1.0.min.js"></script>
      <script type="text/javascript" src="../../../js/materialize.min.js"></script>
      <script type="text/javascript" src="index.js"></script>
    </body>
  </html>