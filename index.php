<?php
    session_start();
    $_SESSION['timeout'] = time();

    if(isset($_SESSION['uname'])) {
        unset($_SESSION['uname']);
    }

    include_once(__DIR__."/includes/general.config.php");

?>
    <html>

    <head>
        <link rel="icon" href="./favicon.ico">
        <title><?php echo $TITLE_TEXT;?> Login</title>
        <!-- BASIC SETUP (DO NOT CHANGE) -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="js/jquery-3.1.0.min.js"></script>
        <script src="index.elab.js" type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
        <!-- DONT CHANGE ABOVE IT -->
    </head>
    <style>
        .container {
            margin-top: 80px;
        }        
        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
            margin-left: 40px;
        }
        
        .form input {
            font-size: 1.2em;
            font-weight: 500;
        }
        
        .main-logo {
            margin-bottom: 50px;
        }
        
        .login {
            padding: 20px;
            font-size: 1.3em;
            margin-bottom: 15px;
        }
        
        .seperator {
            width: 100%;
            border-bottom: 1px solid;
            border-color: #cfd8dc;
            clear: both;
        }
        
        #button {
            display: inline-block;
        }
        
        #error {
            margin-top: 10px;
        }
        
        .foot-text {
            padding: 14px;
        }
        #about,#reg {
            cursor: pointer;
            text-transform: uppercase;
        }
    
        .text {
            padding-top: 115px;
            font-size: 1.2em;
        }
        .side-nav .userView a {
            padding: 3px;
        }
        .side-nav a {
            font-weight: 400;
            cursor: pointer;
        }
        .row {
            margin-bottom: 0px;
        }
	.space-top {
	    margin-top: 10px;
	}
    .btnn {
        margin-top: 10px;
        margin-right: 10px;
    }

        #modal-footer-msg {
		font-size: 13px;
        }
	#modal-content-msg {
		font-size: 17px;
	}
    #medal {
        margin-left: 25px;
    }
    #ranks {
        font-size: 17px;
    }
       
	.reg {
		text-transform: uppercase;
		margin-top: 10px !important;
		margin-bottom: 10px !important;
	}
    </style>

    <body>
        <nav>
            <div class="nav-wrapper  indigo darken-1">
                <a href="<?php echo $HREF_URL; ?>"><img id="image" class="brand-logo logo-img s2" src="logo.png"> </img></a>
                <a href="#" class="brand-logo  center hide-on-med-and-down"><?php echo $NAVBAR_TEXT; ?></a>
            </div>
        </nav>


   <div id="modal1" class="modal">
        <div class="modal-content">
          <h4 class="center-align"> <?php echo $TITLE_TEXT; ?></h4>

          <p class="space-top" id="modal-content-msg"> <?php echo $TITLE_TEXT; ?> is an auto evaluation tool for learning programming. 
    	  <?php echo $TITLE_TEXT; ?> helps learners to practice and acquire programming skills.
    	  Learning to program helps a person to become more logical, creative and innovative.</p><br>
          <div class="left">
          <a class="space-top white-text text-darken-3 btn pink left-align" id="ranks" target="_blank" href="ranks">LEADERBOARD</a>
          <a class="space-top white-text text-darken-3 btn pink left-align" id="ranks" target="_blank" href="ranks/top100">TOP 100 CODERS</a>
          </div>

          <p class="space-top red-text text-darken-3 right-align" id="modal-footer-msg">* For any Queries Contact Your Respective Faculty Advisor</p>

        </div>
        <div class="modal-footer">
          <a href="#!" class=" modal-action modal-close waves-effect waves-blue btn-flat">CLOSE</a>
        </div>
   </div>

    <div id="modal2" class="modal">
        <div class="modal-content center">
            <h5 class="center-align"> REGISTRATION</h5>

            <br>
            <a class="waves-effect waves-light pink btn-large" href="./register/register.php">Student Register</a>
            <a class="waves-effect waves-light green btn-large" href="./facultyregister">Faculty Register</a>
        </div>
        <div class="modal-footer">
          <a href="#!" class=" modal-action modal-close waves-effect center waves-blue btn-flat">CLOSE</a>
        </div>
    </div>

<div class="container">
    <div class="row">
        <div class="col s12 l4 offset-l4 m10 offset-m1">
            
        <div class="card">
            <div id="elabsi" class="login indigo darken-1 white-text"><?php echo $TITLE_TEXT; ?> SIGN IN</div>
            <div class="card-content">
                    <div class="form ">
                        <div class="row">
                            <div class="col s12 input-field">
                                <!-- Username --><input id="username" type="text" name="username" class="validate">
                                <label for="username">Registration Number/Faculty ID</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 input-field">
                                <!-- Password --><input id="password" type="password" name="password" class="validate">
                                <label for="password">Password</label>
                            </div>
                                <div class="center  red-text text-darken-2" id="error">
                                    <p id="error"></p>
                                </div>
                                <div class="progress pink lighten-3">
                                    <div class="indeterminate pink darken-1"></div>
                                </div>
                                <script>
                                    $(".progress").hide();
                                </script>
                            <a class="waves-effect btnn waves-light btn pink right" id="button"><b>Login</b></a>
                        </div>
                    </div>
                </div>
                
                <div class="seperator "></div>
                
                <div class="aboutDiv center">

                                <a class="foot-text grey-text text-darken-1 left" data-activates="slide-out" id="about">About <?php echo $TITLE_TEXT; ?></a>
                                <a class="foot-text grey-text text-darken-1 right" data-activates="slide-out" id="reg">Register?</a>
                </div>

                <div class="seperator "></div>


                <!--ORIGNAL CODE <a href="register/forgot_password.php" class="forgot left foot-text">Forgot Password ?</a> 
                 
                
                <div class="center">
            		<div class="reg" id="about">About eLab</div>
            		<div class="seperator"></div>
            		<div class="reg" ><a class="black-text" href="./register/register.php"> Student Register </a> </div>
            		<div class="seperator"></div>
            		<div class="reg" ><a href="./facultyregister" class="black-text"> Faculty Register </a></div>
            		<div class="seperator"></div>
            	</div> -->
                

                <div class="seperator "></div>
                <div style="clear:both"></div>

            </div>
        </div>
    </div>
</div>





        <!-- BASIC SETUP (DO NOT CHANGE) -->
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <!-- DONT CHANGE ABOVE IT -->
    </body>
    </html>
